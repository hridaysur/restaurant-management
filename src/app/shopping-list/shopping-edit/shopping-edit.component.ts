import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Ingrediant } from '../../shared/ingrediant.model';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  @ViewChild('f') slForm: NgForm;
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingrediant;

  constructor(private slService:ShoppingListService) { }

  ngOnInit() {
    this.subscription = this.slService.StartedEditing.subscribe(
      (index:number) => {
        this.editedItemIndex = index;
        this.editMode = true;
        this.editedItem = this.slService.getIngrediant(index);
        this.slForm.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        });
      }
    );
  }

  onAddItem(form: NgForm) {
      const value = form.value;
    const newIngridiant = new Ingrediant(value.name, value.amount);
    if(this.editMode) {
      this.slService.updateIngrediant(this.editedItemIndex,newIngridiant);
    } else {
    this.slService.addIngrediants(newIngridiant);
  }
  this.editMode = false;
  form.reset();
}

onClear(){
  this.editMode = false;
  this.slForm.reset();
}

onDelete(){
  this.slService.deleteIngrediant(this.editedItemIndex);
  this.onClear();
}

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
