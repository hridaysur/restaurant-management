import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingrediant } from '../shared/ingrediant.model';
import { ShoppingListService} from './shopping-list.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

  ingrediants: Ingrediant[];
  subscription: Subscription;

  constructor(private slService:ShoppingListService) { }

  ngOnInit() {
    this.ingrediants = this.slService.getIngrediants();
    this.subscription = this.slService.IngrediantsChange.subscribe(
      (ingrediant:Ingrediant[]) => {
        this.ingrediants = ingrediant;
      }
    )
  }

  onClickItem(index:number){
    this.slService.StartedEditing.next(index);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }


}
