import { Ingrediant } from '../shared/ingrediant.model';
import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';


export class ShoppingListService {

    IngrediantsChange = new Subject<Ingrediant[]>();
    StartedEditing = new Subject<number>();

    private ingrediants: Ingrediant[] = [
        new Ingrediant('carot', 5),
        new Ingrediant('Avocado', 1)
    ];

    getIngrediants(){
       return this.ingrediants.slice();
    }

    getIngrediant(index:number){
      return this.ingrediants[index];
    }

    addIngrediants(item:Ingrediant){
        this.ingrediants.push(item);
        this.IngrediantsChange.next(this.ingrediants.slice());
    }

    addIngrediantsToSL(ingrediant:Ingrediant[]){
        this.ingrediants.push(...ingrediant);
        this.IngrediantsChange.next(this.ingrediants.slice());
    }

    deleteIngrediant(index:number){
      this.ingrediants.splice(index, 1);
      this.IngrediantsChange.next(this.ingrediants.slice());
    }

    updateIngrediant(index:number, newIngrediant:Ingrediant){
      this.ingrediants[index] = newIngrediant;
      this.IngrediantsChange.next(this.ingrediants.slice());
    }
}
