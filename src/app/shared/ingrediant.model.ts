export class Ingrediant {
    public name : string;
    public amount : number; 

    constructor(name:string , amount : number){
        this.amount=amount;
        this.name = name;
    }
}