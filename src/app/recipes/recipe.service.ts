import { Recipe } from './recipe.model';
import {  Injectable } from '@angular/core';
import { Ingrediant } from '../shared/ingrediant.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';


@Injectable()
export class RecipeService {
    
   
    private recipes:Recipe[] =[
        new Recipe('Wolfe Bowl',
        'a first salad that created by Mandys Sister',
        'http://www.zuuchini.com/wp-content/uploads/IMG_20151023_203254.jpg',
        [
            new Ingrediant('Avocado',1),
            new Ingrediant('Carots',2.5),
            new Ingrediant('Tomatoes',2.5),
            new Ingrediant('Parmesan',2.5),
            new Ingrediant('Sesame Seeds',1),
        ]),
        new Recipe('Tokyo salad',
        'a first salad that created by Mandys Sister',
        'http://www.zuuchini.com/wp-content/uploads/IMG_20151023_203254.jpg',
        [
            new Ingrediant('Avocado',1),
            new Ingrediant('Carots',2.5),
            new Ingrediant('Cucs',2.5),
            new Ingrediant('Brocoli',2.5),
            
        ])
      ];

    constructor(private slService:ShoppingListService){

    }  

    getRecipes(){
        return this.recipes.slice();
    }

    getRecipe(id:number){
        return this.recipes.slice()[id];
    }

    addIngrediantsToSL(ingrediants:Ingrediant[]){
        this.slService.addIngrediantsToSL(ingrediants);
    }
}