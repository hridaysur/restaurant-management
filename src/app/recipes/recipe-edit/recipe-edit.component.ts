import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  id: number;
  editMode = false;
  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.initForm();
      }
    );
  }
  onSubmit() {
    console.log(this.recipeForm);
  }

  private initForm() {

    let name = '';
    let description = '';
    let imagePath = '';
    let recipeIngredients = new FormArray([]);

    if(this.editMode){
    const recipe = this.recipeService.getRecipe(this.id);
      name = recipe.name;
      description = recipe.description;
      imagePath = recipe.imagePath;
      if (recipe['ingrediant']) {
        for (let i of recipe.ingrediant) {
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(i.name),
              'amount': new FormControl(i.amount)
            })
          );
        }
      }
    }

    this.recipeForm = new FormGroup({
      'name' : new FormControl(name),
      'imagePath' : new FormControl(imagePath),
      'description': new FormControl(description),
      'ingrediants': recipeIngredients
    });
  }

  onAddIngredients() {
    (<FormArray>this.recipeForm.get('ingrediants')).push(
      new FormGroup({
        'name': new FormControl(),
        'amount': new FormControl()
      })
    );
  }

}
